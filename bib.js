var ToolBar_Supported = ToolBar_Supported ;
if (ToolBar_Supported != null && ToolBar_Supported == true)
{
	//To Turn on/off Frame support, set Frame_Supported = true/false.
	Frame_Supported = false;

	// Customize default ICP menu color - bgColor, fontColor, mouseoverColor
	setDefaultICPMenuColor("blue", "white", "yellow");

	// Customize toolbar background color
	//setToolbarBGColor("white");

	// display ICP Banner
	//setICPBanner("/imagens/logo_top.gif","","Logo do BoavistaNet") ;
	
	//***** Add ICP menus *****
	//Saldos
	addICPMenu("MenuSaldos", "Saldos", "","");
	addICPSubMenu("MenuSaldos","Conta Corrente    ","");
	addICPSubMenu("MenuSaldos","Poupan�a          ","");
	addICPSubMenu("MenuSaldos","Investimentos     ","");
	
	//Extratos
	addICPMenu("MenuExtratos", "Extratos", "","");
	addICPSubMenu("MenuExtratos","Conta Corrente","");

	//Transfer�ncias
	addICPMenu("MenuTransferencias", "Transfer&ecirc; ncias", "","");
	addICPSubMenu("MenuTransferencias","Entre C/C","");
	addICPSubMenu("MenuTransferencias","de C/C para Poupan&ccedil;a","");
	addICPSubMenu("MenuTransferencias","de Poupan�a para C/C","");
	addICPSubMenu("MenuTransferencias","Envio de DOC D","");
	addICPSubMenu("MenuTransferencias","Envio de DOC E","");


	//Pagamentos
	addICPMenu("MenuPagamentos", "Pagamentos", "","");
	addICPSubMenu("MenuPagamentos","Cobran&ccedil; a Boavista","");
	addICPSubMenu("MenuPagamentos","Cobran&ccedil; a Outros Bancos","");
	addICPSubMenu("MenuPagamentos","Contas (Luz, G&aacute;s, Tel, &Aacute;gua)","");

	//Servi�os
	addICPMenu("MenuServicos", "Servi�os", "","");
	addICPSubMenu("MenuServicos","Consulta de dados cadastrais","");
	addICPSubMenu("MenuServicos","Atualiza��o de senha","");
	addICPSubMenu("MenuServicos","Tal�o de Cheques","");
	addICPSubMenu("MenuServicos","Hist�rico","");

	//Hist�rico
	addICPMenu("MenuInvestimentos", "Investimentos", "","");

	//Encerrar
	addICPMenu("MenuEncerrar", "Encerrar", "","");

}
