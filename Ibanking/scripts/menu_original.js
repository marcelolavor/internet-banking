var ToolBar_Supported = ToolBar_Supported;
if (ToolBar_Supported != null && ToolBar_Supported == true)
{
	
	// Atribuir as cores do Menu - bgColor, fontColor, mouseoverColor
	setDefaultICPMenuColor("#C6C3B5", "BLACK", "WHITE"); //#EDE1D1 #E5E3D5 #EAE8D7 #EBE4CF #C6C3B5

	// Atribuir a cor de Fundo do Menu
	setToolbarBGColor("");

	// Atribuir a fonte do Menu
	setMenuFont("ARIAL");

	// Atribuir o tamanho da fonte do Menu
	setMenuFontSize("1");

	// Atribuir o Bold para a fonte do Menu
	setMenuFontBold("1");

	// Atribuir o Itálico para a fonte do Menu
	//setMenuFontItalic("1");
	
	//***** Adicionar Menus e SubMenus *****
	//Saldos
	Menu("Saldos", "SALDOS", "Hint do Saldo","");
	SubMenu("Saldos","Conta Corrente","saldo_cc_11.asp");
	SubMenu("Saldos","Poupan&ccedil;a","saldo_poup_11.asp");
	SubMenu("Saldos","Investimentos","saldo_invest_11.asp");
	
	//Extratos
	Menu("Extratos", "EXTRATOS", "","");
	SubMenu("Extratos","Conta Corrente","extrato_cc_12.asp");

	//Extratos
	Menu("TESTE", "TESTE", "","");
	SubMenu("TESTE","TESTE 1","extrato_cc_12.asp");

	//Transferências
	Menu("Transferencias", "TRANSFER&Ecirc;NCIAS", "","");
	SubMenu("Transferencias","Entre C/C","transf_cc_cc_13.asp");
	SubMenu("Transferencias","de C/C para Poupan&ccedil;a","transf_cc_poup_13.asp");
	SubMenu("Transferencias","de Poupan&ccedil;a para C/C","transf_poup_cc_13.asp");
	SubMenu("Transferencias","Envio de DOC D","doc_d_13.asp");
	SubMenu("Transferencias","Envio de DOC E","doc_e_13.asp");


	//Pagamentos
	Menu("Pagamentos", "PAGAMENTOS", "","");
	SubMenu("Pagamentos","Cobran&ccedil;a Boavista","pag_bloquetos_14.asp");
	SubMenu("Pagamentos","Cobran&ccedil;a Outros Bancos","pag_bloquetos_outros_bancos_13.asp");
	SubMenu("Pagamentos","Contas (Luz, G&aacute;s, Tel, &Aacute;gua)","pag_concessionarias_13.asp");

	//Servicos
	Menu("Servicos", "SERVI&Ccedil;OS", "","");
	SubMenu("Servicos","Consulta de dados cadastrais","dados_cadastrais_12.asp");
	SubMenu("Servicos","Atualização de senha","manutencao_senha_12.asp");
	SubMenu("Servicos","Tal&atilde;o de Cheques","talao_cancelamento_12.asp");
	SubMenu("Servicos","Hist&oacute;rico","historico_12.asp");
	SubMenu("Servicos","Bancos","bancos_11.asp");
	SubMenu("Servicos","Concession&aacute;rias","concessionarias_11.asp");
	
	//Investimentos
	Menu("Investimentos", "INVESTIMENTOS", "","");
	SubMenu("Investimentos","Fundos","apliresg_fundos_13.asp");
	
	//Encerrar
	Menu("Encerrar", "ENCERRAR", "","encerrar.asp");

}