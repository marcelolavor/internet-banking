// Inicializa��o de Vari�veis
var ToolBar_Supported = false;
var Frame_Supported   = false;
var DoInstrumentation = false;

// Gera Trace para Debug
function trace(msg) {
	alert(msg)
}

// Verificar se a vers�o do Browser � maior do que 3
if (navigator.userAgent.indexOf("MSIE")    != -1 && 
	navigator.userAgent.indexOf("Windows") != -1 && 
	navigator.appVersion.substring(0,1) > 3)
{
	ToolBar_Supported = true;
}

// Inicializar vari�veis do ToolBar
if (ToolBar_Supported)
{
	var newLineChar = String.fromCharCode(10);
	var char34 = String.fromCharCode(34);
	var LastMSMenu = "";
	var CurICPMenu = "";
	var IsMSMenu = false;
	var IsMenuDropDown = true;
	var HTMLStr;
	var x = 0;
	var y = 0;
	var x2 = 0;
	var y2 = 0;
	var MSMenuWidth;
	var ToolbarMinWidth;
	var ToolbarMenu;
	var ToolbarBGColor;
	var ToolbarLoaded = false;
	var aDefMSColor  = new Array(3);
	var aDefICPColor = new Array(3);
	var aCurMSColor  = new Array(3);
	var aCurICPColor = new Array(3);
	var MSFont;
	var ICPFont;
	var ICPFontSize;
	var ICPFontBold;
	var ICPFontItalic;
	var MaxMenu = 30;
	var TotalMenu = 0;
	var arrMenuInfo = new Array(30);

	// Enviar as Tags de inicio do ToolBar
	document.write("<SPAN ID='StartMenu' STYLE='display:none;'></SPAN>");

	// Concatenar o template do ToolBar
	HTMLStr = 
		"<DIV ID='idToolbar'     STYLE='background-color:white;width:100%'>" +
		"<DIV ID='idRow1'        STYLE='position:relative;height:0;Align:center;'>" +
		"<DIV ID='idICPBanner'   STYLE='position:absolute;top:0;left:0;height:60;width:250;overflow:hidden;vertical-align:top;'><!--BEG_ICP_BANNER--><!--END_ICP_BANNER--></DIV>" +
		"</DIV>" +
		"<DIV ID='idRow2' STYLE='position:relative;left:250;height:0;'>" +
		"<DIV ID='idADSBanner'   STYLE='position:absolute;top:0;left:0;height:40;width:200;vertical-align:top;overflow:hidden;'><!--BEG_ADS_BANNER--><!--END_ADS_BANNER--></DIV>" +
		"</DIV>" +
		"<DIV ID='idRow3' STYLE='position:relative;height:20;width:100%'>" +
		"<DIV align='center' ID='idICPMenuPane' STYLE='position:absolute;top:0;left:0;height:20;background-color:black;' NOWRAP><!--ICP_MENU_TITLES--></DIV>" +
		"</DIV>" +
		"</DIV>" +
		"<SCRIPT TYPE='text/javascript'>" + 
		"   var ToolbarMenu = StartMenu;" + 
		"</SCRIPT>" + 
		"<DIV WIDTH=100%>";

	//trace (HTMLStr);

	// Definir o handler do evento resize
	window.onresize  = resizeToolbar;

	// Inicializar vari�veis globais
	ToolbarBGColor	= "white";		// Cor do fundo do ToolBar
	MSFont  = "xx-small Verdana";		// Fonte do Menu MS
	ICPFont = "bold xx-small Verdana";	// Fonte do Menu ICP
	ICPFontSize = "1";	// Tamanho da Fonte do Menu
	ICPFontBold = "0";  // BOLD da fonte do Menu
	ICPFontItalic = "0";  // It�lico da fonte do Menu
		
	// Defini��o das cores default do menu principal
	aDefMSColor[0]	= aCurMSColor[0]  = "black";	// Cor de Fundo;
	aDefMSColor[1]	= aCurMSColor[1]  = "white";	// Cor da Fonte
	aDefMSColor[2]  = aCurMSColor[2]  = "red";	// Cor da Fonte no mouseover

	// Defini��o das cores default do menu lateral
	aDefICPColor[0]	= aCurICPColor[0] = "green";	// Cor de Fundo;
	aDefICPColor[1] = aCurICPColor[1] = "white";	// Cor da Fonte
	aDefICPColor[2] = aCurICPColor[2] = "red";	// Cor da Fonte no mouseover
}



function drawToolbar()
{
	HTMLStr += "</DIV>";

	// Enviar o c�digo do template
	document.write(HTMLStr);

	// Atribuir o indicador de ToolBar carregado
	ToolbarLoaded = true;

	// Calcular maior valor para a dimens�o do menu
	// MSMenuWidth     = Math.max(idMSMenuPane.offsetWidth, (200+112));
	// ToolbarMinWidth = (250+18) + MSMenuWidth;

	idToolbar.style.backgroundColor     = ToolbarBGColor;
	idICPMenuPane.style.backgroundColor = aDefICPColor[0];
	resizeToolbar();

	for (i = 0; i < TotalMenu; i++) 
	{
		thisMenu = document.all(arrMenuInfo[i].IDStr);
		if (thisMenu != null)
		{
			if (arrMenuInfo[i].IDStr == LastMSMenu && arrMenuInfo[i].type == "R")
			{
				//Last MSMenu has to be absolute width
				arrMenuInfo[i].type = "A";
				arrMenuInfo[i].unit = 200;
			}
			if (arrMenuInfo[i].type == "A")
				thisMenu.style.width = arrMenuInfo[i].unit;
			else 
				thisMenu.style.width = Math.round(arrMenuInfo[i].width * arrMenuInfo[i].unit) + 'em';
		}
	}
}

function resizeToolbar()
{
	if (ToolBar_Supported == false) return;

	w = Math.max(ToolbarMinWidth, document.body.clientWidth) - ToolbarMinWidth;
	
	// idMSMenuCurve.style.left  = (250+w);
	// idMSMenuPane.style.left   = (250+w+18);
	// idMSMenuPane.style.width  = MSMenuWidth;

	// idADSBanner.style.left    = (w+18);

	// idMSCBanner.style.left    = (w+18+200);
	// idMSCBanner.style.width   = (MSMenuWidth - 200);
	
	// idICPMenuPane.style.width = ToolbarMinWidth + w;
}

function setToolbarBGColor(color)
{	
	ToolbarBGColor = color;
	if (ToolbarLoaded == true)
		idToolbar.style.backgroundColor = ToolbarBGColor;
}	


function setMenuFont(sFont)
{	ICPFont = sFont;
}

function setMenuFontSize(sFontSize)
{	ICPFontSize = sFontSize;
}

function setMenuFontBold(sFontBold)
{	ICPFontBold = sFontBold;
}

function setMenuFontItalic(sFontItalic)
{	ICPFontItalic = sFontItalic;
}

function setDefaultICPMenuColor(bgColor, fontColor, mouseoverColor)
{	
	if (bgColor   != "")	  aDefICPColor[0] = bgColor;
	if (fontColor != "")	  aDefICPColor[1] = fontColor;
	if (mouseoverColor != "") aDefICPColor[2] = mouseoverColor;
}

function setICPMenuColor(MenuIDStr, bgColor, fontColor, mouseoverColor)
{	
	if (ToolbarLoaded == false) return;

	// Reset previous ICP Menu color if any
	if (CurICPMenu != "")
	{
		PrevID = CurICPMenu.substring(4);
		CurICPMenu = "";
		setICPMenuColor(PrevID, aDefICPColor[0], aDefICPColor[1], aDefICPColor[2]);
	}

	var	id = "AM_" + "ICP_" + MenuIDStr;
	var thisMenu = document.all(id);
	if (thisMenu != null)
	{
		CurICPMenu = "ICP_" + MenuIDStr;
		aCurICPColor[0] = bgColor;
		aCurICPColor[1] = fontColor;
		aCurICPColor[2] = mouseoverColor;

		// Change menu color
		if (bgColor != "")
			thisMenu.style.backgroundColor = bgColor;
		if (fontColor != "")
			thisMenu.style.color = fontColor;

		// Change subMenu color
		id = "ICP_" + MenuIDStr;
		thisMenu = document.all(id);
		if (thisMenu != null)
		{
			if (bgColor != "")
				thisMenu.style.backgroundColor = bgColor;
			
			if (fontColor != "")
			{
				i = 0;
				id = "AS_" + "ICP_" + MenuIDStr;
				thisMenu = document.all.item(id,i);
				while (thisMenu != null)
				{
					thisMenu.style.color = fontColor;
					i += 1;
					thisMenu = document.all.item(id,i);
				}
			}
		}
	}
}

function setAds(Gif,Url,AltStr)
{	setBanner(Gif,Url,AltStr,"<!--BEG_ADS_BANNER-->","<!--END_ADS_BANNER-->");
}

function setICPBanner(Gif,Url,AltStr)
{	setBanner(Gif,Url,AltStr,"<!--BEG_ICP_BANNER-->","<!--END_ICP_BANNER-->");
}

function setMSBanner(Gif,Url,AltStr)
{	tempGif = "/library/toolbar/images/" + Gif;
	setBanner(tempGif,Url,AltStr,"<!--BEG_MSC_BANNER-->","<!--END_MSC_BANNER-->");
}

function setBanner(vaGif, vaUrl, vaAltStr, vaInicioTag, vaFinalTag)
{
	PosInicial = HTMLStr.indexOf(vaInicioTag);
	PosFinal = HTMLStr.indexOf(vaFinalTag) + vaFinalTag.length;
	
	SubStr = HTMLStr.substring(PosInicial, PosFinal);
	SrcStr = "";
	if (vaUrl != "")
		SrcStr += "<A Target='_top' HREF='" + formatURL(vaUrl, vaGif) + "'>";
	SrcStr += "<IMG SRC='" + vaGif + "' ALT='" + vaAltStr + "' BORDER=0>";
	if (vaUrl != "")
		SrcStr += "</A>";
	SrcStr = vaInicioTag + SrcStr + vaFinalTag;
	HTMLStr = HTMLStr.replace(SubStr, SrcStr);	
}

function setICPSubMenuWidth(MenuIDStr, WidthType, WidthUnit)
{	tempID = "ICP_" + MenuIDStr;
	setSubMenuWidth(tempID, WidthType, WidthUnit);
}

function setSubMenuWidth(MenuIDStr, WidthType, WidthUnit)
{
	var fFound = false;
	if (TotalMenu == MaxMenu)
	{
		alert("Unable to process menu. Maximum of " + MaxMenu + " reached.");
		return;
	}
	
	for (i = 0; i < TotalMenu; i++)
		if (arrMenuInfo[i].IDStr == MenuIDStr)
		{
			fFound = true;
			break;
		}

	if (!fFound)
	{
		arrMenuInfo[i] = new menuInfo(MenuIDStr);
		TotalMenu += 1;
	}

	if (!fFound && WidthType.toUpperCase().indexOf("DEFAULT") != -1)
	{
		arrMenuInfo[i].type = "A";
		arrMenuInfo[i].unit = 160;
	}
	else
	{
		arrMenuInfo[i].type = (WidthType.toUpperCase().indexOf("ABSOLUTE") != -1)? "A" : "R";
		arrMenuInfo[i].unit = WidthUnit;
	}
}

// This function creates a menuInfo object instance.
function menuInfo(MenuIDStr)
{
	this.IDStr = MenuIDStr;
	this.type  = "";
	this.unit  = 0;
	this.width = 0;
	this.count = 0;
}

function updateSubMenuWidth(MenuIDStr)
{
	for (i = 0; i < TotalMenu; i++)
		if (arrMenuInfo[i].IDStr == MenuIDStr)
		{
			if (arrMenuInfo[i].width < MenuIDStr.length) 
				arrMenuInfo[i].width = MenuIDStr.length;
			arrMenuInfo[i].count = arrMenuInfo[i].count + 1;
			break;
		}
}

function Menu(MenuIDStr, MenuDisplayStr, MenuHelpStr, MenuURLStr)
{ 	
	if (Menu.arguments.length > 4)
		TargetStr = Menu.arguments[4];
	else
		TargetStr = "_top";
	tempID = "ICP_" + MenuIDStr;
	addMenu(tempID, MenuDisplayStr, MenuHelpStr, MenuURLStr, TargetStr, true); 
}

function addMenu(MenuIDStr, MenuDisplayStr, MenuHelpStr, MenuURLStr, TargetStr, bICPMenu)
{
	var cFontBold1 = "";
	var cFontBold2 = "";
	var cFontItalic1 = "";
	var cFontItalic2 = "";
	
	cColor0 = bICPMenu? aDefICPColor[0] : aDefMSColor[0];
	cColor1 = bICPMenu? aDefICPColor[1] : aDefMSColor[1];
	cColor2 = bICPMenu? aDefICPColor[2] : aDefMSColor[2];
	tagStr  = bICPMenu? "<!--ICP_MENU_TITLES-->" : "<!--MS_MENU_TITLES-->";

	cFont   = bICPMenu? ICPFont : MSFont;
	cFontSize = bICPMenu? ICPFontSize : "2";

	if (bICPMenu) {
		cFontBold1 = ICPFontBold == "1"? "<b>": "";
		cFontBold2 = ICPFontBold == "1"? "</b>": "";
	}

	if (bICPMenu) {
		cFontItalic1 = ICPFontItalic == "1"?  "<i>": "";
		cFontItalic2 = ICPFontItalic == "1"?  "</i>": "";
	}

	MenuStr = newLineChar;
	if (bICPMenu == false && LastMSMenu != "")
		MenuStr += "<SPAN STYLE='font:" + cFont + ";fontsize:" + cFontSize + ";color:" + 
					cColor1 + "'><font face='" + cFont + "' size=" + cFontSize + ">" + 
					cFontBold1 + cFontItalic1 + "|&nbsp;" + cFontItalic2 + cFontBold2 + 
					"</font></SPAN>"; 
	MenuStr += "<A TARGET='" + TargetStr + "' TITLE='" + MenuHelpStr + "'" +
			   "   ID='AM_" + MenuIDStr + "'" +
			   "   STYLE='text-decoration:none;cursor:hand;font:" + cFont + ";fontsize:" + cFontSize + ";background-color:" + cColor0 + ";color:" + cColor1 + ";'";
	if (MenuURLStr != "")
	{
		if (bICPMenu)
			MenuStr += " HREF='/" + formatURL(MenuURLStr, ("ICP_" + MenuDisplayStr)) + "'";
		else
			MenuStr += " HREF='" + formatURL(MenuURLStr, ("MS_" + MenuDisplayStr)) + "'";
	}
	else
		MenuStr += " HREF='' onclick='window.event.returnValue=false;'";
	MenuStr += 	" onmouseout="  + char34 + "mouseMenu('out' ,'" + MenuIDStr + "'); hideMenu();" + char34 + 
				" onmouseover=" + char34 + "mouseMenu('over','" + MenuIDStr + "'); doMenu('"+ MenuIDStr + "');" + char34 + ">" +
				"<font face='" + cFont + "' size=" + cFontSize + ">" + cFontBold1 + cFontItalic1 + "&nbsp;" + MenuDisplayStr + "&nbsp;" + cFontItalic2 + cFontBold2 + "</font></a>";
	if (bICPMenu)
		MenuStr += "<SPAN STYLE='font:" + cFont + ";fontsize:" + cFontSize + ";color:" + cColor1 +
				   "'><font face='" + cFont + "' size=" + cFontSize + ">" + 
				   cFontBold1 + cFontItalic1 + "&nbsp;|" + cFontItalic2 + cFontBold2 + "</font></SPAN>";
	MenuStr += tagStr;

	HTMLStr = HTMLStr.replace(tagStr, MenuStr);	
	//trace (HTMLStr);

	setSubMenuWidth(MenuIDStr,"default",0);
}

function SubMenu(MenuIDStr, SubMenuStr, SubMenuURLStr)
{	
	if (SubMenu.arguments.length > 3)
		TargetStr = SubMenu.arguments[3];
	else
		TargetStr = "_top";
	tempID = "ICP_" + MenuIDStr;
	addSubMenu(tempID,SubMenuStr,SubMenuURLStr,TargetStr,true); 
}

function addSubMenu(MenuIDStr, SubMenuStr, SubMenuURLStr, TargetStr, bICPMenu)
{

	var cFontBold1 = "";
	var cFontBold2 = "";
	var cFontItalic1 = "";
	var cFontItalic2 = "";

	cFont   = bICPMenu? ICPFont : MSFont;
	cColor0 = bICPMenu? aDefICPColor[0] : aDefMSColor[0];
	cColor1 = bICPMenu? aDefICPColor[1] : aDefMSColor[1];
	cColor2 = bICPMenu? aDefICPColor[2] : aDefMSColor[2];

	cFont   = bICPMenu? ICPFont : MSFont;
	cFontSize = bICPMenu? ICPFontSize : "2";

	if (bICPMenu) {
		cFontBold1 = ICPFontBold == "1"? "<b>": "";
		cFontBold2 = ICPFontBold == "1"? "</b>": "";
	}

	if (bICPMenu) {
		cFontItalic1 = ICPFontItalic == "1"?  "<i>": "";
		cFontItalic2 = ICPFontItalic == "1"?  "</i>": "";
	}
	
	var MenuPos = MenuIDStr.toUpperCase().indexOf("MENU");
	if (MenuPos == -1) { MenuPos = MenuIDStr.length; }
	InstrumentStr = MenuIDStr.substring(0 , MenuPos) + "|" + SubMenuStr;;
	URLStr        = formatURL(SubMenuURLStr, InstrumentStr);

	var LookUpTag  = "<!--" + MenuIDStr + "-->";
	var sPos = HTMLStr.indexOf(LookUpTag);

	if (sPos <= 0)
	{
		HTMLStr += newLineChar + newLineChar +
				"<SPAN ID='" + MenuIDStr + "'" +
				" STYLE='display:none;position:absolute;width:160;background-color:" + cColor0 + ";padding-top:0;padding-left:0;padding-bottom:20;z-index:9;'" +
				" onmouseout='hideMenu();'>";
		if (Frame_Supported == false || bICPMenu == false)
			HTMLStr += "<HR  STYLE='position:absolute;left:0;top:0;color:" + cColor1 + "' SIZE=1>";
		HTMLStr += "<DIV STYLE='position:relative;left:0;top:8;'>";
	}

"<font face='" + cFont + "' size=" + cFontSize + ">" + cFontBold1 + cFontItalic1 
	TempStr = newLineChar +
				"<A ID='AS_" + MenuIDStr + "'" +
				"   STYLE='text-decoration:none;cursor:hand;font:" + cFont + ";color:" + cColor1 + "'" +
				"   HREF='/servicos/" + URLStr + "' TARGET='" + TargetStr + "'" +
				" onmouseout="  + char34 + "mouseMenu('out' ,'" + MenuIDStr + "');" + char34 + 
				" onmouseover=" + char34 + "mouseMenu('over','" + MenuIDStr + "');" + char34 + ">" +
				"<font face='" + cFont + "' size=" + cFontSize + ">" + cFontBold1 + cFontItalic1 +
				"&nbsp;" + SubMenuStr + cFontItalic2 + cFontBold2 + "</font></A><BR>" + LookUpTag;

	if (sPos <= 0)
		HTMLStr += TempStr + "</DIV></SPAN>";
	else
		HTMLStr = HTMLStr.replace(LookUpTag, TempStr);

	//trace (HTMLStr);
	updateSubMenuWidth(MenuIDStr);	
}

function addICPSubMenuLine(MenuIDStr)
{	
	tempID = "ICP_" + MenuIDStr;
	addSubMenuLine(tempID,true);
}

function addSubMenuLine(MenuIDStr, bICPMenu)
{
	var LookUpTag = "<!--" + MenuIDStr + "-->";
	var sPos = HTMLStr.indexOf(LookUpTag);
	if (sPos > 0)
	{
		cColor  = bICPMenu? aDefICPColor[1] : aDefMSColor[1];
		TempStr = newLineChar + "<HR STYLE='color:" + cColor + "' SIZE=1>" + LookUpTag;
		HTMLStr = HTMLStr.replace(LookUpTag, TempStr);
	}
}

function mouseMenu(id, MenuIDStr) 
{
	IsMSMenu   = (MenuIDStr.toUpperCase().indexOf("MS_") != -1);
	IsMouseout = (id.toUpperCase().indexOf("OUT") != -1);

	if (IsMouseout)
	{
		color = IsMSMenu? aDefMSColor[1] : aDefICPColor[1];
		if (MenuIDStr == CurICPMenu && aCurICPColor[1] != "") 
			color = aCurICPColor[1];
	}
	else
	{
		color = IsMSMenu? aDefMSColor[2] : aDefICPColor[2];
		if (MenuIDStr == CurICPMenu && aCurICPColor[2] != "") 
			color = aCurICPColor[2];
	}
	window.event.srcElement.style.color = color;
}

function doMenu(MenuIDStr) 
{

	var thisMenu = document.all(MenuIDStr);
	if (ToolbarMenu == null || thisMenu == null || thisMenu == ToolbarMenu) 
	{
		window.event.cancelBubble = true;
		return false;
	}

	// Reset dropdown menu
	window.event.cancelBubble = true;
	ToolbarMenu.style.display = "none";
	showElement("SELECT");
	showElement("OBJECT");
	ToolbarMenu = thisMenu;
	IsMSMenu = (MenuIDStr.toUpperCase().indexOf("MS_") != -1);

	// Set dropdown menu display position
	x  = window.event.srcElement.offsetLeft +
	 	 window.event.srcElement.offsetParent.offsetLeft;
	if (MenuIDStr == LastMSMenu) 
		x += (window.event.srcElement.offsetWidth - thisMenu.style.posWidth);
	x2 = x + window.event.srcElement.offsetWidth;
	y  = (IsMSMenu)? 
		 (idRow1.offsetHeight) :
		 (idRow1.offsetHeight + idRow2.offsetHeight + idRow3.offsetHeight);
	thisMenu.style.top  = y;
	thisMenu.style.left = x;
	thisMenu.style.clip = "rect(0 0 0 0)";
	thisMenu.style.display = "block";

	// delay 2 millsecond to allow the value of ToolbarMenu.offsetHeight be set
	window.setTimeout("showMenu()", 2);
	return true;
}

function showMenu() 
{
	if (ToolbarMenu != null) 
	{ 
		IsMenuDropDown = (Frame_Supported && IsMSMenu == false)? false : true;
		if (IsMenuDropDown == false)
		{
			y = (y - ToolbarMenu.offsetHeight - idRow3.offsetHeight);
			if (y < 0) y = 0;
			ToolbarMenu.style.top = y;
		}
		y2 = y + ToolbarMenu.offsetHeight;

		ToolbarMenu.style.clip = "rect(auto auto auto auto)";
		hideElement("SELECT");
		hideElement("OBJECT");
	}
}

function hideMenu()
{
	if (ToolbarMenu != null && ToolbarMenu != StartMenu) 
	{
		// Don't hide the menu if the mouse move between the menu and submenus
		cY = event.clientY + document.body.scrollTop;
		if ( (event.clientX >= (x+5) && event.clientX <= x2) &&
			 ((IsMenuDropDown == true  && cY > (y-10) && cY <= y2)      ||
			  (IsMenuDropDown == false && cY >= y     && cY <= (y2+10)) ))
		{
			window.event.cancelBubble = true;
			return; 
		}

		ToolbarMenu.style.display = "none";
		ToolbarMenu = StartMenu;
		window.event.cancelBubble = true;

		showElement("SELECT");
		showElement("OBJECT");
	}
}

function hideElement(elmID)
{
	for (i = 0; i < document.all.tags(elmID).length; i++)
	{
		obj = document.all.tags(elmID)[i];
		if (! obj || ! obj.offsetParent)
			continue;

		// Find the element's offsetTop and offsetLeft relative to the BODY tag.
		objLeft   = obj.offsetLeft;
		objTop    = obj.offsetTop;
		objParent = obj.offsetParent;
		while (objParent.tagName.toUpperCase() != "BODY")
		{
			objLeft  += objParent.offsetLeft;
			objTop   += objParent.offsetTop;
			objParent = objParent.offsetParent;
		}
		// Adjust the element's offsetTop relative to the dropdown menu
		objTop = objTop - y;

		if (x > (objLeft + obj.offsetWidth) || objLeft > (x + ToolbarMenu.offsetWidth))
			;
		else if (objTop > ToolbarMenu.offsetHeight)
			;
		else if (IsMSMenu && (y + ToolbarMenu.offsetHeight) <= 80)
			;
		else
			obj.style.visibility = "hidden";
	}
}

function showElement(elmID)
{
	for (i = 0; i < document.all.tags(elmID).length; i++)
	{
		obj = document.all.tags(elmID)[i];
		if (! obj || ! obj.offsetParent)
			continue;
		obj.style.visibility = "";
	}
}

function formatURL(URLStr, InstrumentStr)
{
	var tempStr = URLStr;

	if (DoInstrumentation && URLStr != "" )
	{
		var ParamPos1 = URLStr.indexOf("?");
		var ParamPos2 = URLStr.lastIndexOf("?");
		var ParamPos3 = URLStr.toLowerCase().indexOf("target=");
		var ParamPos4 = URLStr.indexOf("#");
		var Bookmark  = "";
		var URL = URLStr;
		if (ParamPos4 >= 0)
		{
		 	URL = URLStr.substr(0, ParamPos4);
			Bookmark = URLStr.substr(ParamPos4);
		}
		
		if (ParamPos1 == -1)
			tempStr = "?MSCOMTB=";
		else if (ParamPos1 == ParamPos2 && ParamPos3 == -1)	
			tempStr = "&MSCOMTB=";
		else if (ParamPos1 == ParamPos2 && ParamPos3 != -1)	
			tempStr = "?MSCOMTB=";
		else if (ParamPos1 < ParamPos2)
			tempStr = "&MSCOMTB=";

		tempStr = URL + tempStr + InstrumentStr.replace(" ","%20") + Bookmark;
	}
	return tempStr;
}