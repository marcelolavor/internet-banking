<% Language=VBScript %>
<% servico = "SALDO PARCIAL DE CONTA CORRENTE" %>
<!-- #Include File="../includes/begin.inc" -->
<!-- #Include File="../includes/popup.inc" -->
<!-- #Include File="../includes/banner.inc" -->
<!-- #Include File="../includes/head.inc" -->
<table border="0" cellpadding="0" cellspacing="0" width="42%">
  <tr>
    <td width="4%">&nbsp;</td>
    <td width="96%"> 
      <table border=0 cellpadding=0   width=100% cellspacing=0>
        <tr> 
          <td align=left width=68%><font face=Arial size=2><strong>Saldo Parcial</strong></font></td>
          <td align=right width=30%><font size=2 face=Arial> 42.529,23</font></td>
          <td align=left width=2%>&nbsp;</td>
        </tr>
        <tr> 
          <td align=left width=68%><font face=Arial size=2><strong>- Saldo Bloqueado</strong></font></td>
          <td align=right width=30%><font size=2 face=Arial> 999,88</font></td>
          <td align=left width=2%>&nbsp;</td>
        </tr>
        <tr> 
          <td align=left width=68%><font face=Arial size=2><strong>- CPMF Provisionado</strong></font></td>
          <td align=right width=30%><font size=2 face=Arial> 669,94</font></td>
          <td align=left width=2%>&nbsp;</td>
        </tr>
        <tr> 
          <td align=left width=68%><font face=Arial size=2><strong>+ Limite CEB</strong></font></td>
          <td align=right width=30%><font size=2 face=Arial> 91.000,00</font></td>
          <td align=left width=2%>&nbsp;</td>
        </tr>
        <tr> 
          <td align=left width=68%><font face=Arial size=2><strong>+ Saldo p/ 
            resgate autom�tico</strong></font></td>
          <td align=right width=30%><font size=2 face=Arial> 2.236,16</font></td>
          <td align=left width=2%>&nbsp;</td>
        </tr>
        <tr> 
          <td align=left width=68%><font face=Arial size=2><strong>= Saldo Dispon�vel 
            para Saque</strong></font></td>
          <td align=right width=30%><font size=2 face=Arial> 134.095,57</font></td>
          <td align=left width=2%>&nbsp;</td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td width="4%">&nbsp;</td>
    <td width="96%"> 
      <hr></td></tr>
  <tr>
    <td width="4%">&nbsp;</td>
    <td width="96%"> 
      <table border=0 cellpadding=0 width=100% cellspacing=0>
        <tr> 
          <td colspan=4 width=100% align=center><font face=Arial, helveticasans-serif size=2><b>Investimentos 
            Autom�ticos</b></font></td>
        </tr>
        <tr> 
          <td colspan=4 width=100% align=left>&nbsp; </td>
        </tr>
        <tr> 
          <td colspan=4 width=100% align=center> 
            <table border=0 cellpadding=0 width=100% cellspacing=0>
              <tr> 
                <td width=78%><font face=Arial size=2><b>FBAF</b></font></td>
                <td align=right width=20%><font face=Arial size=2> 221,16</font></td>
                <td align=left width=2%></td>
              </tr>
              <tr> 
                <td width=78%><font face=Arial size=2><b>PIB</b></font></td>
                <td align=right width=20%><font face=Arial size=2> 543,00</font></td>
                <td align=left width=2%></td>
              </tr>
              <tr> 
                <td width=78%><font face=Arial size=2><b>FIFCP</b></font></td>
                <td align=right width=20%><font face=Arial size=2> 889,33</font></td>
                <td align=left width=2%></td>
              </tr>
              <tr> 
                <td width=78%><font face=Arial size=2><b>FIFLIQ</b></font></td>
                <td align=right width=20%><font face=Arial size=2> 459,17</font></td>
                <td align=left width=2%></td>
              </tr>
              <tr> 
                <td width=78%><font face=Arial size=2><b>Poupan�a Autom�tica</b></font></td>
                <td align=right width=20%><font face=Arial size=2> 123,50</font></td>
                <td align=left width=2%></td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td width="4%">&nbsp;</td>
    <td width="96%"> 
      <hr></td></tr>
  <tr>
    <td width="4%">&nbsp;</td>
    <td align="center" width="96%"><font size="1" face="Arial, Helvetica, sans-serif">Informa��es 
      sujeitas a altera��o at� o final do expediente.</font></td>
  </tr>
</table>
<br>
<!-- #Include File="../includes/menurodape.inc" -->
<!-- #Include File="../includes/end.inc" -->