<% Language=VBScript %>
<% servico = "MANUTEN&Ccedil;&Atilde;O DE SENHA" %>
<!-- #Include File="../includes/begin.inc" -->
<!-- #Include File="../includes/head.inc" -->
<table border="0" cellspacing="0" cellpadding="0" width="100%">
  <tr> 
    <td width="100%"> 
      <hr>
    </td>
  </tr>
  <tr> 
    <td> 
      <p align="center"><font face="Arial, Helvetica, sans-serif" size="2"><b><font color="#004040">Atualiza��o 
        de Senha BoavistaNet</font></b></font>
    </td>
  </tr>
  <tr> 
    <td width="100%">
      <hr>
    </td>
  </tr>
  <tr>
    <td width="100%">&nbsp; </td>
  </tr>
  <tr> 
    <td width="100%" height="173"> 
      <table border="0" width="100%" cellspacing="0" cellpadding="0">
        <tr> 
          <td width="50%" align="center" valign="top"> 
            <p align="left"><font size="2" face="Arial, Helvetica, sans-serif">Para 
              alterar sua senha do BoavistaNet, primeiramente digite sua senha 
              do Cart&atilde;o de D&eacute;bito Boavista.<br>
              <br>
              A senha do BoavistaNet pode ser alfanum&eacute;rica e composta no 
              m&iacute;nimo por<br>4 posi&ccedil;&otilde;es e no m&aacute;ximo por 8.</font></p>
            <p align="left"><font face="Arial, Helvetica, sans-serif" size="2">A 
              guarda da senha para acesso ao <b><font size="2" face="Arial, Helvetica, sans-serif"><font color="#004040">BoavistaNet 
              - Internet Banking</font></font></b> &eacute; de exclusiva 
              responsabilidade do Cliente.</font></p>
            <p align="left"><font face="Arial, Helvetica, sans-serif" size="2">Por 
              quest&otilde;es de seguran&ccedil;a, sugerimos substitu&iacute;-la 
              periodicamente. N&atilde;o caber&aacute; ao Banco Boavista nenhuma 
              responsabilidade pelo seu uso indevido.</font></p>
          </td>
          <td width="50%" align="center" valign="top"> 
            <form method="POST" action="salvocgi.exe" name="salvo">
              <table border="0"
      cellpadding="0" cellspacing="0" width="100%">
                <tr> 
                  <td width="10%" height="2" nowrap></td>
                  <td width="90%" align="left" height="2" nowrap><font face="Arial" color="#000000"><small><strong><font size="2">Senha 
                    do Cart�o Boavista:</font></strong></small></font></td>
                </tr>
                <tr> 
                  <td width="10%" height="2"></td>
                  <td width="90%" align="left" height="2"><font face="Arial" color="#000000"><font face="Arial, Helvetica, sans-serif"><font size="2"> 
                    </font><font face="Arial" color="#000000"><font face="Arial, Helvetica, sans-serif"><font size="2">
                    <input type="password" name="senhacartao" size="8">
                    </font></font></font><font size="2"> </font></font></font> 
                  </td>
                </tr>
                <tr> 
                  <td width="10%" height="2"></td>
                  <td width="90%" align="left" height="2">&nbsp;</td>
                </tr>
                <tr> 
                  <td width="10%" height="2" nowrap>&nbsp;</td>
                  <td width="90%" align="left" height="2" nowrap><font face="Arial" color="#000000"><small><strong><font size="2">Nova 
                    Senha BoavistaNET:</font></strong></small></font></td>
                </tr>
                <tr> 
                  <td width="10%" height="2">&nbsp;</td>
                  <td width="90%" align="left" height="2"><font face="Arial" color="#000000"><font size="2"><font face="Arial, Helvetica, sans-serif"> 
                    </font><font face="Arial" color="#000000"><font size="2"><font face="Arial, Helvetica, sans-serif">
                    <input type="password" name="senha" size="8">
                    </font></font></font><font face="Arial, Helvetica, sans-serif"> 
                    </font></font></font></td>
                </tr>
                <tr> 
                  <td width="10%" height="2">&nbsp;</td>
                  <td width="90%" align="left" height="2">&nbsp;</td>
                </tr>
                <tr> 
                  <td width="10%" height="2">&nbsp;</td>
                  <td width="90%" align="left" height="2"><font face="Arial" color="#000000"><small><strong><font size="2">Confirma��o 
                    da Nova Senha BoavistaNET:</font></strong></small></font></td>
                </tr>
                <tr> 
                  <td width="10%" height="2">&nbsp;</td>
                  <td width="90%" align="left" height="2"><font face="Arial" color="#000000"><font face="Arial, Helvetica, sans-serif"><font size="2"> 
                    </font><font face="Arial" color="#000000"><font face="Arial, Helvetica, sans-serif"><font size="2">
                    <input type="password" name="senha2" size="8">
                    </font></font></font><font size="2"> </font></font></font></td>
                </tr>
                <tr> 
                  <td width="10%" height="2">&nbsp;</td>
                  <td width="90%" align="left" height="2">&nbsp;</td>
                </tr>
                <tr>
                  <td width="10%" height="2">&nbsp;</td>
                  <td width="90%" align="left" height="2">
                    <div align="left"><font face="Arial" color="#000000"><small> 
                      <input type="image" name="Enviar"
      src="imagens/enviar.gif" align="top" border="0" width="86" height="20" tabindex="3"
      alt="Enviar">
                      <input type="hidden" name="SALVO_InfoRule"
      value="BIB_Manutencao_senha_22IB(senhacartao,senha,senha2)">
                      </small></font></div>
                  </td>
                </tr>
              </table>
              </form>
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr> 
    <td width="100%" align="center" height="2"> </td>
  </tr>
</table>
<script language=JavaScript>
document.salvo.senhacartao.focus()
</script>
<!-- #Include File="../includes/menurodape.inc" -->
<!-- #Include File="../includes/end.inc" -->