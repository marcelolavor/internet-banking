<% Language=VBScript %>
<% servico = "DOC E" %>
<!-- #Include File="../includes/begin.inc" -->
<!-- #Include File="../includes/head.inc" -->
<table border="0" width="100%" cellspacing="0" cellpadding="0" height="350">
  <tr><td><hr></td></tr>
  <tr>
    <td align="center"><strong><font face="Arial, Helvetica, sans-serif" size="2" color="#004040">DOC 
      &quot;E&quot; - Transfer�ncia para Conta de Terceiros </font></strong></td>
  </tr>
  <tr><td><hr></td></tr>
  <tr> 
    <td><font face="Arial" size="2"><b>Titular:</b>&nbsp;SalvoImport(1,1,1)<br>
      <b>Ag�ncia:</b>&nbsp;SalvoImport(1,1,2) - SalvoImport(1,1,3)SalvoImport(1,1,4)&nbsp;&nbsp;&nbsp;&nbsp; 
      <b> C/C:</b>&nbsp;SalvoImport(1,1,5)</font></td>
  </tr>
  <tr><td><hr></td></tr>
  <tr>
    <td><form method="POST" action="salvocgi.exe" name="salvo">
	<table border="0" width="100%" cellspacing="0" cellpadding="0">
	<tr><td>
        <table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td width="13%"><font face="Arial" size="2"><b>Banco</b></font></td>
            <td width="13%"><font face="Arial" size="2"><b>Ag�ncia</b></font><font face="Times New Roman, Times, serif" size="2">*</font></td>
            <td width="29%"><font face="Arial" size="2"><b>Conta</b></font><font face="Times New Roman, Times, serif" size="2">**</font></td>
            <td width="6%">&nbsp;</td>
            <td width="39%"><font face="Arial" size="2"><b>Valor</b></font></td>
          </tr>
          <tr>
            <td width="13%" align="left"><font face="Arial" size="2">
              <input type="text" name="banco" size="4" maxlength="4" style="font-family: Arial; font-size: 10pt"></font></td>
            <td width="13%"><font face="Arial" size="2"><input type="text" name="agencia" size="4" maxlength="4" style="font-family: Arial; font-size: 10pt"></font></td>
            <td width="29%" height="2"><font face="Arial" size="2"><input type="text" name="conta" size="13" maxlength="13" style="font-family: Arial; font-size: 10pt"></font></td>
            <td width="6%" height="2" align="right" valign="middle"><font face="Arial" size="2">R$ </font></td>
	    <td width="39%" height="2" align="left" valign="top"><font face="Arial" size="2"><input type="text" name="valor" size="15" maxlength="15" style="font-family: Arial; font-size: 10pt"></font></td>
          </tr>
        </table>
	</td></tr>
	<tr><td>
	<table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr> 
            <td valign="bottom"><font face="Arial" size="2" color="#000000"><b>CPF</b></font></td>
            <td valign="bottom" width="49%"><font face="Arial" size="2" color="#000000"><b>CGC</b></font></td>
          </tr>
          <tr>
            <td valign="bottom"><font face="Arial" size="2"><input type="text" style="font-family: Arial; font-size: 10pt" name="cpf" size="9" maxlength="9"> - <input type="text" name="cpf_dv" style="font-family: Arial; font-size: 10pt"size="2" maxlength="2"></font></td>
            <td valign="bottom" width="49%" align="left"><font face="Arial" size="2"><input type="text" name="cgc" style="font-family: Arial; font-size: 10pt"size="10" maxlength="8"> / <input type="text" name="cgc_inter" size="4" style="font-family: Arial; font-size: 10pt" maxlength="4"> - <input type="text" style="font-family: Arial; font-size: 10pt" name="cgc_dv" size="2" maxlength="2"></font></td>
          </tr>
        </table>
	</td></tr>
	<tr>
            <td height="2"> 
              <table border="0" cellspacing="0" cellpadding="0" width="100%">
                <tr> 
                  <td><font face="Arial" size="2"><b>Favorecido:</b></font></td>
                  <td><font face="Arial, Helvetica, sans-serif" size="2"> 
                    <input type="text" name="favorecido" style="font-family: Arial; font-size: 10pt" size="35" maxlength="35">
                    </font></td>
                </tr>
                <tr> 
                  <td><font face="Arial" size="2"><b>Finalidade:</b></font></td>
                  <td><font face="Arial" size="2"><b>
                    <select name="finalidade" size="1">
                      <option>01 - Cr&eacute;dito em Conta Corrente </option>
                      <option>02 - Pagamento Aluguel/Condom&iacute;nios </option>
                      <option>03 - Pagamento de Duplicatas/T&iacute;tulos </option>
                      <option>04 - Pagamento de Dividendos </option>
                      <option>05 - Pagamento de Mensalidades Escolares </option>
                      <option>06 - Pagamento de Sal&aacute;rios </option>
                      <option>07 - Pagamento de Fornecedores/Honor&aacute;rios 
                      </option>
                      <option>08 - Opera&ccedil;&otilde;es de C&acirc;mbio/Fundos/Bolsa 
                      de Valores </option>
                      <option>09 - Repasse de Arrecada&ccedil;&atilde;o/Pagamento 
                      de Tributos </option>
                      <option>11 - DOC para Poupan&ccedil;a </option>
                      <option>12 - DOC para Dep&oacute;sito Judicial</option>
                      <option>13 - Outros </option>
                    </select>
                    </b></font>
                    <input type="hidden" name="SALVO_InfoRule" value="BIB_DOC_E_23IB(banco,agencia,conta,favorecido,cpf,cpf_dv,cgc,cgc_inter,cgc_dv,finalidade,valor)">
                  </td>
                </tr>
              </table>
	</td></tr>
	</table>
        </form>
	</td></tr>
	<tr><td>
        <table border="0" cellspacing="0" cellpadding="0" width="100%">
	  <tr><td width="3%"><font face="Times New Roman, Times, serif" size="2">*<br>**</font></td>
                  <td width="95%"><font face="Arial, Helvetica, sans-serif" size="1">
		Somente os quatro primeiros n&uacute;meros<br>Somente n&uacute;meros</font></td>
          </tr>
        </table>
	</td></tr>
  <tr>
    <td align="center"> <a href="javascript:Enviar('doc')"><img src="imagens/enviar.gif" width="86" height="20" alt="Enviar DOC" name="Enviar" border="0"></a> 
      &nbsp;&nbsp;<font face="Arial" size="2" color="#000000"><b> </b></font>&nbsp;&nbsp; 
      <a href="javascript:Enviar('bancos')"><img src="imagens/consulta.gif" width="86" height="20" border="0" alt="Consultar lista de Bancos"></a> 
    </td>
  </tr>
  <tr><td><hr></td></tr>
  <tr><td>[MSGATENCAO]</td></tr>
  <tr><td>[MSGAGEND1]</td></tr>
  <tr><td>[PHONE]</td></tr>
  <tr><td>[BOTTONMENU]</td></tr>
</table>
<form method="POST" name="form_bancos" action="salvocgi.exe">
<input type="hidden" name="SALVO_InfoRule" value="BIB_Bancos_11IB()"></form>
<script language=JavaScript>
document.salvo.banco.focus()
</script>
<!-- #Include File="../includes/menurodape.inc" -->
<!-- #Include File="../includes/end.inc" -->