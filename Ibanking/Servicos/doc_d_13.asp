<% Language=VBScript %>
<% servico = "DOC D" %>
<!-- #Include File="../includes/begin.inc" -->
<!-- #Include File="../includes/head.inc" -->
<table border="0" cellspacing="0" cellpadding="0" width="100%">
  <tr><td><hr></td></tr>
  <tr>
    <td align="center"><b><font face="Arial, Helvetica, sans-serif" size="2" color="#004040">DOC 
        &quot;D&quot; - Transfer�ncia para Conta de Mesma Titularidade </font></b>
    </td>
  </tr>
  <tr>
    <td>
      <hr>
    </td>
  </tr>
  <tr> 
    <td><font face="Arial" size="2"><b>Titular:</b>&nbsp;SalvoImport(1,1,1)<br>
      <b>Ag�ncia:</b>&nbsp;SalvoImport(1,1,2) - SalvoImport(1,1,3)SalvoImport(1,1,4)&nbsp;&nbsp;&nbsp;&nbsp; 
      <b> C/C:</b>&nbsp;SalvoImport(1,1,5)</font></td>
  </tr>
  <tr>
    <td>
      <hr>
    </td>
  </tr>
  <tr> 
    <td> 
      <form method="POST" name="salvo" action="salvocgi.exe">
        <table border="0" cellspacing="0" cellpadding="0" width="100%">
          <tr> 
            <td width="2%">&nbsp;</td>
            <td width="11%" align="left"> <font face="Arial" size="2"><b>Banco</b></font> 
            </td>
            <td width="17%"> <font face="Arial" size="2"><b>Ag�ncia</b></font><font face="Times New Roman, Times, serif" size="2">* 
              </font></td>
            <td width="31%"> <font face="Arial" size="2"><b>Conta e d&iacute;gito</b></font><font face="Times New Roman, Times, serif" size="2">*</font><font face="Times New Roman, Times, serif" size="2">* 
              </font> </td>
            <td width="4%" align="left" valign="top">&nbsp;</td>
            <td width="35%" align="left" valign="top"> <font face="Arial" size="2"><b>Valor</b></font> 
            </td>
          </tr>
          <tr> 
            <td width="2%">&nbsp;</td>
            <td width="11%" align="left"><font face="Arial" size="2" color="#000000"> 
              <input type="text" name="banco" size="4" maxlength="4" style="font-family: Arial; font-size: 10pt">
              </font></td>
            <td width="17%"><font face="Arial" size="2" color="#000000"> 
              <input type="text" name="agencia" size="4" maxlength="4" style="font-family: Arial; font-size: 10pt">
              </font></td>
            <td width="31%"><font face="Arial" size="2" color="#000000"> 
              <input type="text" name="conta" size="13" maxlength="13" style="font-family: Arial; font-size: 10pt">
              </font> </td>
            <td width="4%" align="right"><font face="Arial" size="2">R$&nbsp;</font></td>
            <td width="35%" align="left"> <font face="Arial" size="2"> 
              <input type="text" name="valor" size="15" maxlength="15"
              style="font-family: Arial; font-size: 10pt">
              </font> </td>
          </tr>
          <tr> 
            <td width="2%">&nbsp;</td>
            <td width="11%" align="left">&nbsp;</td>
            <td width="17%">&nbsp;</td>
            <td width="31%"><font face="Arial" size="2" color="#000000"><b> 
              <input type="hidden" name="SALVO_InfoRule"  value="BIB_DOC_D_23IB(banco, agencia, conta, valor, finalidade)">
              </b></font></td>
            <td width="4%" align="right">&nbsp;</td>
            <td width="35%" align="left">&nbsp;</td>
          </tr>
          <tr> 
            <td width="2%">&nbsp;</td>
            <td width="11%" align="left"><font face="Arial" size="2" color="#000000"><b>Favorecido:&nbsp;</b></font></td>
            <td colspan="4"><font face="Arial" size="2" color="#000000"><b>SalvoImport(1,1,6)</b></font></td>
          </tr>
          <tr> 
            <td width="2%">&nbsp;</td>
            <td width="11%" align="left"><font face="Arial" size="2" color="#000000"><b>Finalidade:&nbsp;</b></font></td>
            <td colspan="4"><font face="Arial" size="2" color="#000000"><b> 
              <select name="finalidade" size="1" style="font-family: Arial; font-size: 10pt">
                <option>01 - Cr&eacute;dito em Conta Corrente </option>
                <option>02 - Pagamento Aluguel/Condom&iacute;nios </option>
                <option>03 - Pagamento de Duplicatas/T&iacute;tulos </option>
                <option>04 - Pagamento de Dividendos </option>
                <option>05 - Pagamento de Mensalidades Escolares </option>
                <option>06 - Pagamento de Sal&aacute;rios </option>
                <option>07 - Pagamento de Fornecedores/Honor&aacute;rios </option>
                <option>08 - Opera&ccedil;&otilde;es de C&acirc;mbio/Fundos/Bolsa 
                de Valores </option>
                <option>09 - Repasse de Arrecada&ccedil;&atilde;o/Pagamento de 
                Tributos </option>
                <option>11 - DOC para Poupan&ccedil;a </option>
                <option>12 - DOC para Dep&oacute;sito Judicial</option>
                <option>13 - Outros </option>
              </select>
              </b></font></td>
          </tr>
        </table>
      </form>
    </td>
  </tr>
  <tr> 
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="2%">&nbsp;</td>
          <td width="3%"><font face="Times New Roman, Times, serif" size="2">*<br>**</font></td>
          <td width="95%"><font face="Arial, Helvetica, sans-serif" size="1">Somente os quatro primeiros n&uacute;meros<br>Somente n&uacute;meros</font></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td align="center"><a href="javascript:Enviar('doc')"><img src="imagens/enviar.gif" width="86" height="20" alt="Enviar DOC" name="Enviar" border="0"></a> 
      &nbsp;<font face="Arial" size="2" color="#000000"><b> </b></font>&nbsp;&nbsp;&nbsp; 
      <a href="javascript:Enviar('bancos')"><img src="imagens/consulta.gif" width="86" height="20" border="0" alt="Consultar lista de Bancos"></a> 
    </td>
  </tr>
  <tr><td><hr></td></tr>
  <tr>
    <td>[MSGATENCAO]</td>
  </tr>
  <tr>
    <td>[MSGAGEND1]</td>
  </tr>
  <tr>
    <td>[PHONE]</td>
  </tr>
  <tr>
    <td>[BOTTONMENU]</td>
  </tr>
</table>
<form method="POST" name="form_bancos" action="salvocgi.exe">
<input type="hidden" name="SALVO_InfoRule" value="BIB_Bancos_11IB()"></form>
<script language=JavaScript>
document.salvo.banco.focus()
</script>
<!-- #Include File="../includes/menurodape.inc" -->
<!-- #Include File="../includes/end.inc" -->