<% Language=VBScript %>
<% servico = "PAGAMENTO DE BLOQUETOS BOAVISTA" %>
<!-- #Include File="../includes/begin.inc" -->
<!-- #Include File="../includes/head.inc" -->
<table border="0" width="100%" cellspacing="0" cellpadding="0">
  <tr> 
    <td height="184"> 
      <table border="0" width="100%" cellspacing="0" cellpadding="0" align="center">
        <tr> 
          <td> 
            <hr>
          </td>
        </tr>
        <tr> 
          <td> 
            <div align="center"><font color="#004040" face="Arial, Helvetica, sans-serif" size="2"><b>Pagamento 
              de Bloquetos de Cobran&ccedil;a Boavista</b></font></div>
          </td>
        </tr>
        <tr> 
          <td height="2"> 
            <hr>
          </td>
        </tr>
        <tr> 
          <td height="5"><font size="2" face="Arial"> <b>Titular:</b>&nbsp;SalvoImport(1,1)<br>
            <b>Ag�ncia:</b>&nbsp;SalvoImport(1,2) - SalvoImport(1,3)&nbsp; SalvoImport(1,4)<b> 
            C/C:</b>&nbsp;SalvoImport(1,5)</font></td>
        </tr>
        <tr> 
          <td height="2"> 
            <hr>
          </td>
        </tr>
        <tr align="center"> 
          <td height="2"><font face="Arial, Helvetica, sans-serif" size="2"> <b>Identifique 
            em seu bloqueto as &aacute;reas brancas do modelo abaixo.<br>
            Elas correspondem aos campos a serem preenchidos</b></font></td>
        </tr>
        <tr> 
          <td width="100%" align="center" height="2">&nbsp;</td>
        </tr>
        <tr>
          <td width="100%" align="center" height="2"><font face="Arial, Helvetica, sans-serif" size="2">Em 
            caso de preenchimento atrav&eacute;s de leitura &oacute;tica, n&atilde;o 
            &eacute; necess&aacute;rio digitar o n&uacute;mero<br>
            do documento. Utilize o campo C&oacute;digo de Barras, posicionando 
            o cursor neste campo.</font></td>
        </tr>
        <tr> 
          <td width="100%" align="center" height="2">&nbsp;</td>
        </tr>
      </table>
    </td>
  </tr>
  <tr> 
    <td height="402"> 
      <form method="POST" action="salvocgi.exe" name="salvo">
        <table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr> 
            <td width="100%" align="center" height="255"> 
              <table border="1" height="100%" width="100%" bgcolor="#FFFFFF" cellspacing="0" cellpadding="0">
                <tr> 
                  <td width="100%" valign="top" height="10"> 
                    <table width="100%" cellspacing="0" cellpadding="0" border="1">
                      <tr> 
                        <td width="9%" height="6" align="center" bgcolor="#F0F4E8"><font face="Arial, Helvetica, sans-serif" size="2"><font face="Times New Roman, Times, serif"><b>BANCO</b></font><b> 
                          </b></font></td>
                        <td width="7%" height="6" align="center" bgcolor="#F0F4E8"><font face="Times New Roman, Times, serif" size="2"><b>xxx-x</b></font></td>
                        <td width="84%" height="6" bgcolor="#F0F4E8" valign="middle"><font face="Arial" size="2"> 
                          <b><font face="Times New Roman, Times, serif"> </font><font face="Arial" size="1">Numero 
                          do documento (Somente para Digita&ccedil;&atilde;o)</font><br>
                          <font face="Arial, Helvetica, sans-serif" size="1"> 
                          <input type="text" name="num1" size="5" maxlength="5">
                          <input type="text" name="num2" size="5" maxlength="5">
                          <input type="text" name="num3" size="5" maxlength="5">
                          <input type="text" name="num4" size="6" maxlength="6">
                          <input type="text" name="num5" size="5" maxlength="5">
                          <input type="text" name="num6" size="6" maxlength="6">
                          <input type="text" name="num7" size="1" maxlength="1">
                          <input type="text" name="num8" size="12" maxlength="12">
                          </font></b></font></td>
                      </tr>
                    </table>
                  </td>
                </tr>
                <tr> 
                  <td width="100%" valign="top" height="185"> 
                    <table width="100%" cellspacing="0" cellpadding="0" border="1">
                      <tr bgcolor="#F0F4E8"> 
                        <td valign="top" height="2" width="68%"><font face="Arial, Helvetica, sans-serif" size="1"><b>Local 
                          de Pagamento<br>
                          </b></font> <b><font size="2" face="Arial, Helvetica, sans-serif">BoavistaNet 
                          Internet Banking</font></b></td>
                        <td valign="top" height="2" width="32%"><font face="Arial, Helvetica, sans-serif" size="1"><b>Vencimento<br>
                          </b></font></td>
                      </tr>
                      <tr bgcolor="#F0F4E8"> 
                        <td valign="top" height="15" width="68%"><font face="Arial, Helvetica, sans-serif" size="1"><b>Cedente<br>
                          </b></font></td>
                        <td valign="top" height="15" width="32%"><b><font size="1" face="Arial, Helvetica, sans-serif">Ag&ecirc;ncia/ 
                          C&oacute;digo Cedente<br>
                          </font><font size="2" face="Times New Roman, Times, serif">xxxxxxxxxxxxxxxx 
                          </font></b></td>
                      </tr>
                      <tr bgcolor="#F0F4E8"> 
                        <td valign="top" height="2" width="68%"> 
                          <table border="1" width="100%" cellspacing="0" cellpadding="0">
                            <tr> 
                              <td width="19%" valign="top" height="2"><b><font size="1" face="Arial, Helvetica, sans-serif">Data 
                                do Doc.</font></b></td>
                              <td width="21%" valign="top" height="2"><b><font size="1" face="Arial, Helvetica, sans-serif">No. 
                                do Doc.</font></b></td>
                              <td width="18%" valign="top" height="2"><b><font size="1" face="Arial, Helvetica, sans-serif">Esp&eacute;cie 
                                Doc.</font></b></td>
                              <td width="12%" valign="top" height="2"><b><font size="1" face="Arial, Helvetica, sans-serif">Aceite</font></b></td>
                              <td width="30%" valign="top" height="2"><b><font size="1" face="Arial, Helvetica, sans-serif">Data 
                                do Proc.<br>
                                </font><font size="2" face="Times New Roman, Times, serif">&nbsp;</font><font size="1" face="Arial, Helvetica, sans-serif"> 
                                </font></b></td>
                            </tr>
                          </table>
                        </td>
                        <td valign="top" height="2" width="32%"><b><font size="1" face="Arial, Helvetica, sans-serif">Nosso 
                          N&uacute;mero<br>
                          </font><font size="2" face="Times New Roman, Times, serif">xxxxxxxxxxxxxxxx</font></b></td>
                      </tr>
                      <tr> 
                        <td valign="top" height="2" width="68%" bgcolor="#F0F4E8"> 
                          <table border="1" width="100%" cellspacing="0" cellpadding="0">
                            <tr> 
                              <td valign="top" width="34%"><b><font size="1" face="Arial, Helvetica, sans-serif">No. 
                                da Conta / Respons.</font></b></td>
                              <td valign="top" width="9%"><font size="1" face="Arial, Helvetica, sans-serif"><b>Carteira</b></font></td>
                              <td valign="top" width="8%"><font face="Arial, Helvetica, sans-serif" size="1"><b>Esp&eacute;cie<br>
                                </b><b><font face="Times New Roman, Times, serif">R$</font></b> 
                                </font></td>
                              <td valign="top" width="12%"><b><font size="1" face="Arial, Helvetica, sans-serif">Quantidade 
                                </font></b></td>
                              <td valign="top" width="37%"><b><font size="1" face="Arial, Helvetica, sans-serif">Valor</font></b></td>
                            </tr>
                          </table>
                        </td>
                        <td valign="top" width="32%" bgcolor="#F0F4E8"> <b><font size="1" face="Arial, Helvetica, sans-serif">(+) 
                          Valor do Documento</font></b></td>
                      </tr>
                      <tr> 
                        <td valign="top" height="63" width="68%" bgcolor="#F0F4E8"><font face="Arial, Helvetica, sans-serif" size="1"><b>Instru&ccedil;&otilde;es:<br>
                          </b> </font></td>
                        <td height="63" width="32%"> 
                          <table border="1" width="100%" cellspacing="0" cellpadding="0">
                            <tr bgcolor="#F0F4E8"> 
                              <td height="2" valign="top"> <b><font size="1" face="Arial, Helvetica, sans-serif">(-) 
                                Desconto </font></b></td>
                            </tr>
                            <tr bgcolor="#F0F4E8"> 
                              <td height="2" valign="top"> <b><font size="1" face="Arial, Helvetica, sans-serif">(-) 
                                Outras dedu&ccedil;&otilde;es / Abatimento </font></b></td>
                            </tr>
                            <tr bgcolor="#F0F4E8"> 
                              <td height="2" valign="top"><b><font size="1" face="Arial, Helvetica, sans-serif">(+) 
                                Mora / Multa / Juros</font></b></td>
                            </tr>
                            <tr bgcolor="#F0F4E8"> 
                              <td height="2" valign="top" ><b><font size="1" face="Arial, Helvetica, sans-serif">(+) 
                                Outros acr&eacute;scimos</font></b></td>
                            </tr>
                            <tr bgcolor="#F0F4E8"> 
                              <td height="16" valign="top" ><font size="1" face="Arial, Helvetica, sans-serif"><b>(=) 
                                Valor cobrado</b></font></td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
                <tr bgcolor="#f0f4e8" valign="middle"> 
                  <td width="100%" height="3"><b><font size="1" face="Arial, Helvetica, sans-serif"> 
                    </font><font face="Arial" size="2"><font face="Times New Roman, Times, serif" size="2"> 
                    </font><font size="1">C&oacute;digo de Barras (Somente para 
                    Leitura &Oacute;tica) </font><br>
                    <font face="Arial, Helvetica, sans-serif" size="2"> 
                    <input type="text" name="codbarra" size="44" maxlength="44">
                    </font></font></b></td>
                </tr>
              </table>
            </td>
          </tr>
          <tr> 
            <td width="100%" align="center" height="2">&nbsp;</td>
          </tr>
          <tr> 
            <td width="100%" align="center" height="2"> 
              <p><font face="Arial, Helvetica, sans-serif" size="2"> </font><font face="Arial, Helvetica, sans-serif" size="2">Limite 
                m&aacute;ximo por bloqueto emitido pelo Boavista -&gt; <b>R$ 10.000,00</b></font><font size="2" face="Arial"> 
                </font></p>
            </td>
          </tr>
          <tr> 
            <td width="100%" align="center" height="2">&nbsp;</td>
          </tr>
          <tr> 
            <td width="100%" align="center" height="2"><font size="2" face="Arial"> 
              <input type="image" name="Enviar" src="imagens/enviar.gif" align="center" border="0" width="86" height="20" tabindex="5" value="enviar" alt="Enviar">
              <input type="hidden" name="SALVO_InfoRule" value="BIB_Pag_bloquetos_24IB(num1,num2,num3,num4,num5,num6,num7,num8,codbarra)">
              </font></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr> 
            <td>[MSGATENCAO]</td>
          </tr>
          <tr> 
            <td>[MSGAGEND1]</td>
          </tr>
          <tr> 
            <td>[PHONE]</td>
          </tr>
          <tr> 
            <td>[BOTTONMENU]</td>
          </tr>
        </table>
      </form>
    </td>
  </tr>
</table>
<script language=JavaScript>
document.salvo.num1.focus()
</script>
<!-- #Include File="../includes/menurodape.inc" -->
<!-- #Include File="../includes/end.inc" -->