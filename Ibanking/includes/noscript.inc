  <table width="92%" border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td width="14%" height="6">&nbsp;</td>
      <td width="86%" height="6">&nbsp;</td>
    </tr>
    <tr> 
      <td width="14%" height="204">&nbsp;</td>
      <td width="86%" height="204"> 
        <p align="center"><font face="Arial, Helvetica, sans-serif" size="2"><b><font color="#004040">Prezado 
          Cliente</font></b></font></p>
        <p><font face="Arial, Helvetica, sans-serif" size="2">Para que voc&ecirc; 
          possa utilizar todas as vantagens do <font color="#004040"><b>BoavistaNet-Internet 
          Banking</b></font>, &eacute; importante voc&ecirc; utilizar, sempre, 
          as &uacute;ltimas vers&otilde;es dos browsers existentes no mercado.</font></p>
        <p><font face="Arial, Helvetica, sans-serif" size="2">Identificamos que 
          o <b>browser utilizado por voc&ecirc; n&atilde;o suporta Java</b>, ou 
          est&aacute; configurado para que n&atilde;o execute o mesmo.<br>
          <br>
          Para que possamos oferecer esse servi&ccedil;o com qualidade e seguran&ccedil;a, 
          pedimos a gentileza de <b>voc&ecirc; atualizar sua vers&atilde;o do 
          navegador</b> ou <b>habilitar a op&ccedil;&atilde;o Java</b> (caso a 
          sua vers&atilde;o seja 3.0 ou superior), escolhendo uma das op&ccedil;&otilde;es 
          abaixo. Cabe lembrar, que os problemas que eventualmente venham a ocorrer 
          com estes navegadores (browsers) ou equipamentos utilizados pelo acesso 
          &agrave; Internet, <b>dever&atilde;o ser solucionados pelos respectivos 
          fornecedores</b>, eximindo o Banco de toda e qualquer responsabilidade 
          por problemas da&iacute; advindos. </font></p>
      </td>
    </tr>
    <tr> 
      <td width="14%">&nbsp;</td>
      <td width="86%">&nbsp;</td>
    </tr>
    <tr align="center"> 
      <td width="14%" height="2">&nbsp;</td>
      <td width="86%" height="2"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td align="center" height="2"><a href="http://www.microsoft.com/brasil/"><font face="Arial, Helvetica, sans-serif" size="2"><b>Internet 
              Explorer</b></font></a></td>
            <td align="center" height="2"><a href="http://www.netscape.com/pt/"><font face="Arial, Helvetica, sans-serif" size="2"><b>Netscape 
              Navigator </b></font></a> </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>